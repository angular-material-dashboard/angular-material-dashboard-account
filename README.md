# Angular Material Dashboard

[![pipeline status](https://gitlab.com/angular-material-dashboard/angular-material-dashboard-account/badges/master/pipeline.svg)](https://gitlab.com/angular-material-dashboard/angular-material-dashboard-account/commits/master)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/0ad28bf1a3144714a34ccc4e11e61c1b)](https://www.codacy.com/app/mostafa.barmshory/angular-material-dashboard-account?utm_source=gitlab.com&amp;utm_medium=referral&amp;utm_content=angular-material-dashboard/angular-material-dashboard-account&amp;utm_campaign=Badge_Grade)

A module to add account setting in angular-material-dashboard. User porfile and
account view is main parts of the module whicd are added directly to user menu.

## Install

This is a bower module and you can install it as follow:

	bower install --save angular-material-dashboard

It is better to address the repository directly

	bower install --save https://gitlab.com/angular-material-dashboard/angular-material-dashboard-account.git

## Document

- [Reference document](https://angular-material-dashboard.gitlab.io/angular-material-dashboard-account/doc/index.html)