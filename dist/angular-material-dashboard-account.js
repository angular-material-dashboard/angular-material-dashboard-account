/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardAccount', [ //
    'ngMaterialDashboard',//
]);

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardAccount')
/*
 * 
 */
.config(function(wbIconServiceProvider) {
	wbIconServiceProvider
	/**
	 * @ngdoc Icons
	 * @name amd-account
	 * @description Current user account
	 */
	.addShape('amd-account', wbIconServiceProvider.getShape('person'))
	/**
	 * @ngdoc Icons
	 * @name amd-profile
	 * @description Current user profile
	 */
	.addShape('amd-profile', wbIconServiceProvider.getShape('person'));
});

///*
// * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//
//angular.module('ngMaterialDashboardAccount')
///*
// * 
// */
//.config(function($routeProvider) {
//	$routeProvider//
//
//	/**
//	 * @ngdoc Routes
//	 * @name /users/profile
//	 * @description Current user profile
//	 * 
//	 * Displays current account profile
//	 */
//	.when('/users/profile', {
//		templateUrl : 'views/amd-account-profile.html',
//		controller : 'AmdProfileController',
//		protect: true,
//		/*
//		 * @ngInject
//		 */
//		integerate: function ($actions){
//			$actions//
//				.group('navigationPathMenu')//
//				.clear();
//			$actions.newAction({
//				id: 'user-account',
//				title: 'Profile',
//				type: 'link',
//				priority : 10,
//				visible : true,
//				url: 'profile',
//				groups: ['navigationPathMenu'],
//			});
//		},
//                helpId: 'dashboard-profile'
//	})
//	
//	/**
//	 * @ngdoc Routes
//	 * @name /users/account
//	 * @description Current account details
//	 * 
//	 * Displays current account details
//	 */
//	.when('/users/account', {
//		templateUrl : 'views/amd-account-account.html',
//		controller : 'MbAccountCtrl',
//		protect: true,
//		/*
//		 * @ngInject
//		 */
//		integerate: function ($actions){
//			$actions.group('navigationPathMenu').clear();
//			$actions.newAction({
//				id: 'user-account',
//				title: 'Account',
//				type: 'link',
//				priority : 10,
//				visible : true,
//				url: 'account',
//				groups: ['navigationPathMenu'],
//			});
//		},
//                helpId: 'dashboard-account'
//	});
//});

/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialDashboardAccount')

/**
 * @ngdoc Controllers
 * @name AmdProfileController
 * @description Manages current user profile
 * 
 */
.controller('AmdProfileController', function (/*$scope, $usr, $window, $navigator, $translate*/) {
//  var ctrl = {
//  saveProfile: false,
//  loadUser: false,
//  loadProfile: false,
//  updatingAvatar: false,
//  user: null,
//  profile: null
//  };
//  $scope.ctrl = ctrl;

//  /**
//  * Sets a user in the scope
//  * 
//  * @param user
//  * @returns
//  */
//  function setUser(user) {
//  ctrl.user = user;
//  // TODO: set page title
//  }

//  /**
//  * Loads user data
//  * 
//  * @returns
//  */
//  function loadUser() {
//  ctrl.loadUser = true;
//  return $usr.getAccount('current')//
//  .then(function (user) {
//  setUser(user);
//  ctrl.loadUser = false;
//  return user;
//  })//
//  .then(function (us) {
//  ctrl.loadProfile = true;
//  return us.getProfiles()//
//  .then(function (profilePag) {
//  ctrl.profiles = profilePag.items;
//  ctrl.profile = ctrl.profiles[0];
//  ctrl.loadProfile = false;
//  return profileList;
//  });
//  })//
//  .catch(function () {
//  ctrl.loadUser = false;
//  ctrl.loadProfile = false;
//  });
//  }

//  /**
//  * Save current user
//  * 
//  * @returns
//  */
//  function save(data) {
//  ctrl.saveProfile = true;
//  return ctrl.user.putProfile(data)//
//  .then(function () {
//  ctrl.saveProfile = false;
//  toast('Your profile updated successfully.');
//  })
//  .catch(function (error) {
//  ctrl.error = error;
//  ctrl.saveProfile = false;
//  });
//  }

//  /**
//  * Update avatar of the current user
//  * 
//  * @name load
//  * @memberof MbAccountCtrl
//  * @returns {promiss} to update avatar
//  */
//  function updateAvatar(avatarFiles) {
//  if (ctrl.updatingAvatar) {
//  return;
//  }
//  ctrl.updatingAvatar = true;
//  return ctrl.user.uploadAvatar(avatarFiles[0].lfFile)//
//  .then(function () {
//  // TODO: hadi 1397-03-02: only reload avatar image by clear and set
//  // (again) avatar address in view
//  // clear address before upload and set it again after upload.
//  $window.location.reload();
//  toast($translate.instant('Your avatar updated successfully.'));
//  }, function () {
//  alert($translate.instant('Failed to update avatar'));
//  })//
//  .finally(function () {
//  ctrl.updatingAvatar = false;
//  });
//  }

//  /**
//  * Cancel page
//  * 
//  * @returns
//  */
//  function cancel() {
//  $navigator.openPage('');
//  }

//  $scope.load = loadUser;
//  $scope.save = save;
//  $scope.cancel = cancel;
//  $scope.updateAvatar = updateAvatar;

//  loadUser();
});

///*
// * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//
//angular.module('ngMaterialDashboardAccount')
///*
// * Main actions of the module
// */
//.run(function($actions, $navigator) {
//	/**
//	 * @ngdoc Actions
//	 * @name amd.account.user.account
//	 * @description Open current user account page
//	 */
//	$actions.newAction({
//		id: 'amd.account.user.account',
//		priority: 100,
//		title: 'Account',
//		icon: 'amd-account',
//		description: 'Display account information',
//		action: function(){
//			$navigator.openPage('users/account');
//		},
//		groups: ['mb.user']
//	});
//	/**
//	 * @ngdoc Actions
//	 * @name amd.account.user.profile
//	 * @description Open current user profile page
//	 */
//	$actions.newAction({
//		id: 'amd.account.user.profile',
//		priority: 10,
//		title: 'Profile',
//		icon: 'amd-profile',
//		description: 'Display profile',
//		action: function(){
//			$navigator.openPage('users/profile');
//		},
//		groups: ['mb.user']
//	});
//});
/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';

angular.module('ngMaterialDashboardAccount')
/*
 * Main actions of the module
 */
.run(function($navigator) {
    $navigator.newGroup({
        id: 'current-user',
        title: 'User',
        description: 'A module of dashboard to manage current user.',
        icon: 'person',
        priority: 1
    });
    $navigator.newItem({
        type: 'link',
        groups: ['current-user'],
        link: '/users/profile',
        title: 'Profile',
        icon: 'account_circle'
    });
    $navigator.newItem({
        type: 'link',
        groups: ['current-user'],
        link: '/users/account',
        title: 'Account',
        icon: 'account_box'
    });
    $navigator.newItem({
        type: 'link',
        groups: ['current-user'],
        link: '/users/password',
        title: 'Password',
        icon: 'fingerprint'
    });
});



angular.module('ngMaterialDashboardAccount').run(['$templateCache', function($templateCache) {
  'use strict';

  $templateCache.put('views/amd-account-account.html',
    "<md-content class=md-padding layout-padding flex mb-preloading=ctrl.loadingUser> <div layout-gt-lg=row layout=column>  <section flex-gt-lg=50 md-whiteframe=1 layout=column layout-margin> <h3 translate>Account information</h3> <md-input-container> <label translate>ID</label> <input ng-model=ctrl.user.id disabled> </md-input-container> <md-input-container> <label translate>Username</label> <input ng-model=ctrl.user.login disabled> </md-input-container> </section> </div> <div layout-gt-lg=row layout=column>                             <section mb-preloading=ctrl.changingPassword flex-gt-lg=50 layout=column md-whiteframe=1 layout-margin> <h3 translate>Password settings</h3> <p translate>insert current password and new password to change it.</p> <form name=passForm ng-submit=changePassword(data) layout=column layout-padding> <md-input-container layout-fill> <label translate>current password</label> <input name=oldPass ng-model=data.oldPass type=password required> <div ng-messages=passForm.oldPass.$error> <div ng-message=required>This is required.</div> </div> </md-input-container> <md-input-container layout-fill> <label translate>new password</label> <input name=newPass ng-model=data.newPass type=password required> <div ng-messages=passForm.newPass.$error> <div ng-message=required>This is required.</div> </div> </md-input-container> <md-input-container layout-fill> <label translate>repeat new password</label> <input name=newPass2 ng-model=newPass2 type=password compare-to=data.newPass required> <div ng-messages=passForm.newPass2.$error> <div ng-message=required>This is required.</div> <div ng-message=compareTo>password is not match.</div> </div> </md-input-container> <input hide type=\"submit\"> <div layout=column layout-align=\"center none\" layout-gt-xs=row layout-align-gt-xs=\"end center\"> <md-button class=\"md-raised md-primary\" ng-click=changePassword(data) ng-disabled=passForm.$invalid> <span translate=\"\">change password</span> </md-button> </div> </form> </section> </div> </md-content>"
  );


  $templateCache.put('views/amd-account-profile.html',
    "<md-content class=md-padding layout-padding flex> <div layout-gt-lg=row layout=column>  <section flex-order=-1 flex-gt-lg=50 layout=column md-whiteframe=1 layout-margin mb-preloading=\"ctrl.updatingAvatar || ctrl.loadUser\"> <h3 translate>Avatar</h3> <img style=\"border-radius: 50%\" width=200px height=200px ng-show=!uploadAvatar ng-src=\"/api/v2/user/accounts/{{ctrl.user.id}}/avatar\"> <lf-ng-md-file-input ng-show=uploadAvatar lf-files=avatarFiles accept=image/* progress preview drag> </lf-ng-md-file-input> <div layout=column layout-align=\"center none\" layout-gt-xs=row layout-align-gt-xs=\"end center\"> <md-button ng-show=!uploadAvatar class=\"md-raised md-primary\" ng-click=\"uploadAvatar=true\"> <sapn translate>edit</sapn> </md-button> <md-button ng-show=uploadAvatar class=\"md-raised md-primary\" ng-click=updateAvatar(avatarFiles)>  <sapn translate>save</sapn> </md-button> <md-button ng-show=uploadAvatar class=md-raised ng-click=\"uploadAvatar=false\">  <sapn translate>cancel</sapn> </md-button> </div> </section>  <section flex-gt-lg=50 layout=column md-whiteframe=1 layout-margin mb-preloading=\"ctrl.loadProfile || ctrl.saveProfile || ctrl.loadUser\"> <h3 translate>Profile</h3> <form name=contactForm layout=column layout-padding> <md-input-container layout-fill> <label translate>First name</label> <input ng-model=ctrl.profile.first_name> </md-input-container> <md-input-container layout-fill> <label translate>Last name</label> <input ng-model=ctrl.profile.last_name> </md-input-container> <md-input-container layout-fill> <label translate>Public email</label> <input name=email ng-model=ctrl.profile.email type=email> <div ng-messages=contactForm.email.$error> <div ng-message=email>Email is not valid.</div> </div> </md-input-container> <md-input-container layout-fill> <label translate>Language</label> <input ng-model=ctrl.profile.language> </md-input-container> <md-input-container layout-fill> <label translate>Timezone</label> <input ng-model=ctrl.profile.timezone> </md-input-container> </form> <div layout=column layout-align=\"center none\" layout-gt-xs=row layout-align-gt-xs=\"end center\"> <md-button class=\"md-raised md-primary\" ng-click=save(ctrl.profile)> <sapn translate>update</sapn> </md-button> </div> </section> </div></md-content>"
  );

}]);
