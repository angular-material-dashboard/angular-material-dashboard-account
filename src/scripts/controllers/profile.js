/*
 * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
 * 
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * 
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * 
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
'use strict';
angular.module('ngMaterialDashboardAccount')

/**
 * @ngdoc Controllers
 * @name AmdProfileController
 * @description Manages current user profile
 * 
 */
.controller('AmdProfileController', function (/*$scope, $usr, $window, $navigator, $translate*/) {
//  var ctrl = {
//  saveProfile: false,
//  loadUser: false,
//  loadProfile: false,
//  updatingAvatar: false,
//  user: null,
//  profile: null
//  };
//  $scope.ctrl = ctrl;

//  /**
//  * Sets a user in the scope
//  * 
//  * @param user
//  * @returns
//  */
//  function setUser(user) {
//  ctrl.user = user;
//  // TODO: set page title
//  }

//  /**
//  * Loads user data
//  * 
//  * @returns
//  */
//  function loadUser() {
//  ctrl.loadUser = true;
//  return $usr.getAccount('current')//
//  .then(function (user) {
//  setUser(user);
//  ctrl.loadUser = false;
//  return user;
//  })//
//  .then(function (us) {
//  ctrl.loadProfile = true;
//  return us.getProfiles()//
//  .then(function (profilePag) {
//  ctrl.profiles = profilePag.items;
//  ctrl.profile = ctrl.profiles[0];
//  ctrl.loadProfile = false;
//  return profileList;
//  });
//  })//
//  .catch(function () {
//  ctrl.loadUser = false;
//  ctrl.loadProfile = false;
//  });
//  }

//  /**
//  * Save current user
//  * 
//  * @returns
//  */
//  function save(data) {
//  ctrl.saveProfile = true;
//  return ctrl.user.putProfile(data)//
//  .then(function () {
//  ctrl.saveProfile = false;
//  toast('Your profile updated successfully.');
//  })
//  .catch(function (error) {
//  ctrl.error = error;
//  ctrl.saveProfile = false;
//  });
//  }

//  /**
//  * Update avatar of the current user
//  * 
//  * @name load
//  * @memberof MbAccountCtrl
//  * @returns {promiss} to update avatar
//  */
//  function updateAvatar(avatarFiles) {
//  if (ctrl.updatingAvatar) {
//  return;
//  }
//  ctrl.updatingAvatar = true;
//  return ctrl.user.uploadAvatar(avatarFiles[0].lfFile)//
//  .then(function () {
//  // TODO: hadi 1397-03-02: only reload avatar image by clear and set
//  // (again) avatar address in view
//  // clear address before upload and set it again after upload.
//  $window.location.reload();
//  toast($translate.instant('Your avatar updated successfully.'));
//  }, function () {
//  alert($translate.instant('Failed to update avatar'));
//  })//
//  .finally(function () {
//  ctrl.updatingAvatar = false;
//  });
//  }

//  /**
//  * Cancel page
//  * 
//  * @returns
//  */
//  function cancel() {
//  $navigator.openPage('');
//  }

//  $scope.load = loadUser;
//  $scope.save = save;
//  $scope.cancel = cancel;
//  $scope.updateAvatar = updateAvatar;

//  loadUser();
});
