///*
// * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//
//angular.module('ngMaterialDashboardAccount')
///*
// * 
// */
//.config(function($routeProvider) {
//	$routeProvider//
//
//	/**
//	 * @ngdoc Routes
//	 * @name /users/profile
//	 * @description Current user profile
//	 * 
//	 * Displays current account profile
//	 */
//	.when('/users/profile', {
//		templateUrl : 'views/amd-account-profile.html',
//		controller : 'AmdProfileController',
//		protect: true,
//		/*
//		 * @ngInject
//		 */
//		integerate: function ($actions){
//			$actions//
//				.group('navigationPathMenu')//
//				.clear();
//			$actions.newAction({
//				id: 'user-account',
//				title: 'Profile',
//				type: 'link',
//				priority : 10,
//				visible : true,
//				url: 'profile',
//				groups: ['navigationPathMenu'],
//			});
//		},
//                helpId: 'dashboard-profile'
//	})
//	
//	/**
//	 * @ngdoc Routes
//	 * @name /users/account
//	 * @description Current account details
//	 * 
//	 * Displays current account details
//	 */
//	.when('/users/account', {
//		templateUrl : 'views/amd-account-account.html',
//		controller : 'MbAccountCtrl',
//		protect: true,
//		/*
//		 * @ngInject
//		 */
//		integerate: function ($actions){
//			$actions.group('navigationPathMenu').clear();
//			$actions.newAction({
//				id: 'user-account',
//				title: 'Account',
//				type: 'link',
//				priority : 10,
//				visible : true,
//				url: 'account',
//				groups: ['navigationPathMenu'],
//			});
//		},
//                helpId: 'dashboard-account'
//	});
//});
