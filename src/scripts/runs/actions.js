///*
// * Copyright (c) 2015-2025 Phoinex Scholars Co. http://dpq.co.ir
// * 
// * Permission is hereby granted, free of charge, to any person obtaining a copy
// * of this software and associated documentation files (the "Software"), to deal
// * in the Software without restriction, including without limitation the rights
// * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// * copies of the Software, and to permit persons to whom the Software is
// * furnished to do so, subject to the following conditions:
// * 
// * The above copyright notice and this permission notice shall be included in all
// * copies or substantial portions of the Software.
// * 
// * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// * SOFTWARE.
// */
//'use strict';
//
//angular.module('ngMaterialDashboardAccount')
///*
// * Main actions of the module
// */
//.run(function($actions, $navigator) {
//	/**
//	 * @ngdoc Actions
//	 * @name amd.account.user.account
//	 * @description Open current user account page
//	 */
//	$actions.newAction({
//		id: 'amd.account.user.account',
//		priority: 100,
//		title: 'Account',
//		icon: 'amd-account',
//		description: 'Display account information',
//		action: function(){
//			$navigator.openPage('users/account');
//		},
//		groups: ['mb.user']
//	});
//	/**
//	 * @ngdoc Actions
//	 * @name amd.account.user.profile
//	 * @description Open current user profile page
//	 */
//	$actions.newAction({
//		id: 'amd.account.user.profile',
//		priority: 10,
//		title: 'Profile',
//		icon: 'amd-profile',
//		description: 'Display profile',
//		action: function(){
//			$navigator.openPage('users/profile');
//		},
//		groups: ['mb.user']
//	});
//});